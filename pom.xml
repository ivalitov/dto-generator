<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.laoruga</groupId>
    <artifactId>dto-generator-parent</artifactId>
    <packaging>pom</packaging>
    <version>1.3.1-SNAPSHOT</version>

    <modules>
        <module>dto-generator</module>
        <module>dto-generator-examples</module>
    </modules>

    <properties>

        <!-- testing -->
        <aspectj.version>1.9.2</aspectj.version>
        <junit.jupiter.version>5.8.2</junit.jupiter.version>
        <junit.platform.version>1.8.2</junit.platform.version>
        <!--   With 17.x.x allure.junit5.version, report from allure-results data is generating empty -->
        <allure.version>2.17.2</allure.version>
        <allure.junit5.version>2.16.1</allure.junit5.version>
        <allure.junit5.assert.version>${allure.version}</allure.junit5.assert.version>
        <allure.maven.version>2.10.0</allure.maven.version>
        <hamcrest.verstion>2.2</hamcrest.verstion>
        <jacoco.version>0.8.7</jacoco.version>

        <argLinePlaceholder />
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <scm>
        <connection>scm:git:git@bitbucket.org:ivalitov/dto-generator.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:ivalitov/dto-generator.git</developerConnection>
        <url>https://bitbucket.org/ivalitov/dto-generator</url>
        <tag>@{project.version}</tag>
    </scm>

    <dependencies>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-text</artifactId>
            <version>1.9</version>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-math3</artifactId>
            <version>3.6.1</version>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.12.0</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.22</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-reload4j</artifactId>
            <version>1.7.36</version>
        </dependency>

        <!-- Junit Platform -->

        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-launcher</artifactId>
            <version>${junit.platform.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Junit Jupiter Engine, Api and Params -->

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Allure Junit5 Interceptor -->

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-junit5</artifactId>
            <version>${allure.junit5.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- Allure Junit5 Aspect li -->

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-junit5-assert</artifactId>
            <version>${allure.junit5.assert.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest</artifactId>
            <version>${hamcrest.verstion}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <profiles>
        <profile>
            <id>coverage</id>
            <properties>
                <argLinePlaceholder>@{argLine}</argLinePlaceholder>
            </properties>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.jacoco</groupId>
                        <artifactId>jacoco-maven-plugin</artifactId>
                        <version>${jacoco.version}</version>
                        <executions>
                            <execution>
                                <id>prepare-agent</id>
                                <goals>
                                    <goal>prepare-agent</goal>
                                </goals>
                            </execution>
                            <execution>
                                <id>report</id>
                                <phase>test</phase>
                                <goals>
                                    <goal>report</goal>
                                </goals>
                                <configuration>
                                    <formats>
                                        <format>XML</format>
                                    </formats>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                            <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                        </manifest>
                    </archive>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>3.0.0-M7</version>
                <configuration>
                    <goal>pre-integration-test</goal>
                    <preparationGoals>package</preparationGoals>
                    <allowTimestampedSnapshots>true</allowTimestampedSnapshots>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                    <remoteTagging>true</remoteTagging>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.2</version>
                <configuration>
                    <argLine>
                        ${argLinePlaceholder}
                        -javaagent:"${settings.localRepository}/org/aspectj/aspectjweaver/${aspectj.version}/aspectjweaver-${aspectj.version}.jar"
                        -Dfile.encoding=UTF-8
                    </argLine>
                    <properties>
                        <!-- Junit 5 Interceptor -->
                        <property>
                            <name>listener</name>
                            <value>io.qameta.allure.junit5.AllureJunit5</value>
                        </property>
                    </properties>
                    <systemProperties>
                        <property>
                            <name>allure.results.directory</name>
                            <value>${project.build.directory}/allure-results</value>
                        </property>
                    </systemProperties>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.aspectj</groupId>
                        <artifactId>aspectjweaver</artifactId>
                        <version>${aspectj.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>io.qameta.allure</groupId>
                <artifactId>allure-maven</artifactId>
                <version>${allure.maven.version}</version>
            </plugin>
        </plugins>
    </build>

</project>